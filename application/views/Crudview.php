<!DOCTYPE html>
<html>
<head>
	<title>Data Mahasiswa</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body style="background-color: grey;">
	<div class="container">
		<h1>
			<br>
		</h1>
		<input class="form-control" id="myInput" type="text" placeholder="Search..">
		<br>
		<table class="table table-dark table-striped" >
			<thead style="background-color: black; text-align: center;">
				<th>No Induk</th>
				<th>Nama</th>
				<th>Alamat</th>
				<th>Action</th>
			</thead>
			<tbody id="myTable">
			<?php foreach ($data as $d){ ?>
				<tr style="text-align: center;" >
					<td><?php echo $d['nim']; ?></td>
					<td><?php echo $d['nama']; ?></td>
					<td><?php echo $d['alamat']; ?></td>
					<td style="text-align: center;">
						<a href="<?php echo base_url()."index.php/crud/edit_data/".$d['nim']; ?>"  type="button" class="btn btn-primary text-white">Edit</a> ||
						<a href="<?php echo base_url()."index.php/crud/do_delete/".$d['nim']; ?>"  type="button" class="btn btn-danger text-white">Delete</a>
					</td>
				</tr>
			<?php } ?>
		</tbody>
		</table>
		<a href="<?php echo base_url()."index.php/crud/add_data/" ?>" type="button" class="btn btn-success text-white">
			Tambah Data
		</a>
		<a href="<?php echo base_url()."index.php/crud/export_excel" ?>" type="button" name="export" class="btn btn-primary text-white" value="Export">
			Export
		</a>
		<a href="<?php echo base_url()."index.php/crud/logout/" ?>" type="button" class="btn btn-danger text-white">
			Logout
		</a>
	</div>

	<script>
		$(document).ready(function(){
			$("#myInput").on("keyup", function() {
				var value = $(this).val().toLowerCase();
				$("#myTable tr").filter(function() {
					$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
				});
			});
		});
	</script>
</body>
</html>