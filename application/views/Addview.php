<!DOCTYPE html>
<html>
<head>
	<title>Data Mahasiswa</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body style="background-color: #323232;">
	<form method="POST" action="<?php echo base_url()."index.php/crud/do_insert" ?>">
		<table class="table table-dark table-striped">
			<tr>
				<td>No Induk</td>
				<td><input type="text" name="nim"></td>
			</tr>
			<tr>
				<td>
					Nama
					<td><input type="text" name="nama"></td>
				</td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td><textarea name="alamat"></textarea></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" class="btn btn-success" name="btnSubmit" value="Simpan"></td>
			</tr>
		</table>
	</form>
</body>
</html>